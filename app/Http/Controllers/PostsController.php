<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;


use \App\Post;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $users = auth()->user()->following->pluck('user_id');
        $posts = Post::wherein('user_id',$users)->with('user')->latest()->get() ;
        return view('posts.index',compact('posts'));
    }

    public function create()
    {
        return view ('posts.create');
    }

    
    public function store(Request $request)
    {
       
        $validator = Validator::make(request()->all(), [
            'caption' => 'required',
            'image' => 'required |image',
            ]);

            $imagePath =  request('image') -> store('uploads','public');
            
            $validatedData = $validator ->valid(); 

        auth()->user()->posts()->create( [
            'caption' => $validatedData['caption'],
            'image' => $imagePath,
        ]);

        return redirect ('/profile/'.auth()->user()->id);

        //dd( $validator -> valid());
        
    }

    public function show(Post $post)
    {
        //dd($post);
        return view ('posts.show',compact('post'));

        // return view ('posts.show',[
        //     'post' => $post
        //     ]);
    }


    public function explore()
    {
        //dd("hello");
        $posts = Post::all();
        return view('posts.explore',compact('posts'));
    }

   

    // public function store(Request $request)
    // {
        // $validatedData =  $this->validate($request,[
        //     'caption' => 'required',
        //     'image' => 'required |image',
        // ]);
        // dd( $validatedData);

    //     // $data = request()->validate([
    //     //     'caption'=>'required',
    //     //     'image'=>'required',
    //     // ]);

    //     $validateData =  $this->validate($request,[
    //         'caption' => 'required',
    //         'image' => 'required |image',
    //     ]);

    //     dd( "hello");
    // }
}
