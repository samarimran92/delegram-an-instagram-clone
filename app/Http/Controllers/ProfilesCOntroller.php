<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Cache; 
use Illuminate\Support\Carbon; 


class ProfilesCOntroller extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(User $user)
    {
        $follows = ( auth()->user() ) ? auth()->user()->following->contains($user->id) : false;
        
        $postCount  = Cache::remember(
            'count.posts'.$user->id,
            \Carbon\Carbon::now()->addSeconds(30),
            function() use ($user) {
                return   $user->posts->count();
            });

        $followersCount  = $user->profile->followers->count();
        $followingCount  = $user->following->count();

        return view('profiles.index',compact('user','follows','postCount','followersCount','followingCount'));
    }

    public function edit(User $user)
    {
        
        //$this->authorize('update', $user->profile);
        return view('profiles.edit',compact('user'));
    }

    public function update(User $user)
    {
        //$this->authorize('update',$user->profile);

        $validator = Validator ::make(request()->all(),[
            'title' =>'required',
            'description' => 'required',
            //'url' => '',
            'url'=> 'url',
            'image' => 'image',
        ]);

        dd($validator->valid());
        $validatedData = $validator->valid();
        //dd(auth()->user());

        if(request('image')){
            $imagePath =  request('image') -> store('profile','public');

            $imageArray = ['image' => $imagePath];
        }
       

        auth()->user()->profile->update(array_merge ( 
            $validatedData,
            $imageArray ??  []
            ));

        return redirect("/profile/{$user->id}");
        //dd($validator->valid());
    }
}
