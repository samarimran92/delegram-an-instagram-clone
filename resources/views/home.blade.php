@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row" style="display:flex">
       
       <div class="col-3 pt-5" >
        <img src="https://instagram.fbah5-1.fna.fbcdn.net/vp/b6c6ea975534170ca3456c2259e7dfe8/5E2B1838/t51.2885-19/s150x150/22709172_932712323559405_7810049005848625152_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net" class="rounded-circle">
       </div>

       <div class="col-9 pt-5" >

            <div class="d-flex justify-content-between align-items-baseline"> 
             <h1>{{$user->username}}</h1>
             <a href="#"> Add New Post</a>
            </div>

            <div style="display:flex;">
                <div class="pr-3"> <strong> 100 </strong>post</div>
                <div class="pr-3"> <strong> 151 </strong>followers </div>
                <div class="pr-3"> <strong> 122 </strong>following </div>
            </div>

            <div class="pt-3"><strong >{{ $user->profile->title }} </strong ></div>
            <div>{{ $user->profile->description }}</div>
            <div><a href>{{ $user->profile->url ?? 'N/A'}}</a></div>

        </div>  
   </div>

   <div class="row  pt-5" style="display:flex;" >
       <div class="col-4 pr-2"  ><img src="https://instagram.fbah5-1.fna.fbcdn.net/vp/69e2d335be2472f213a4b12884ed8622/5E3DFF76/t51.2885-15/e35/s150x150/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 150w,https://instagram.fbah5-1.fna.fbcdn.net/vp/d4490fa9c859761161fa455541fcbadb/5E32613C/t51.2885-15/e35/s240x240/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 240w,https://instagram.fbah5-1.fna.fbcdn.net/vp/095b7f2e86e11ab80c19d3f7fc4cb9f4/5E2C1C86/t51.2885-15/e35/s320x320/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 320w,https://instagram.fbah5-1.fna.fbcdn.net/vp/2e379ff408f5fc7f480b226d199840d7/5E4301DC/t51.2885-15/e35/s480x480/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 480w,https://instagram.fbah5-1.fna.fbcdn.net/vp/74d890cda269b521e7e834777bfd4ea8/5E27D9D1/t51.2885-15/sh0.08/e35/s640x640/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 640w" class="w-100"> </div>
       <div class="col-4 pr-2"  ><img src="https://instagram.fbah5-1.fna.fbcdn.net/vp/69e2d335be2472f213a4b12884ed8622/5E3DFF76/t51.2885-15/e35/s150x150/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 150w,https://instagram.fbah5-1.fna.fbcdn.net/vp/d4490fa9c859761161fa455541fcbadb/5E32613C/t51.2885-15/e35/s240x240/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 240w,https://instagram.fbah5-1.fna.fbcdn.net/vp/095b7f2e86e11ab80c19d3f7fc4cb9f4/5E2C1C86/t51.2885-15/e35/s320x320/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 320w,https://instagram.fbah5-1.fna.fbcdn.net/vp/2e379ff408f5fc7f480b226d199840d7/5E4301DC/t51.2885-15/e35/s480x480/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 480w,https://instagram.fbah5-1.fna.fbcdn.net/vp/74d890cda269b521e7e834777bfd4ea8/5E27D9D1/t51.2885-15/sh0.08/e35/s640x640/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 640w" class="w-100"> </div>
       <div class="col-4 pr-2"  ><img src="https://instagram.fbah5-1.fna.fbcdn.net/vp/69e2d335be2472f213a4b12884ed8622/5E3DFF76/t51.2885-15/e35/s150x150/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 150w,https://instagram.fbah5-1.fna.fbcdn.net/vp/d4490fa9c859761161fa455541fcbadb/5E32613C/t51.2885-15/e35/s240x240/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 240w,https://instagram.fbah5-1.fna.fbcdn.net/vp/095b7f2e86e11ab80c19d3f7fc4cb9f4/5E2C1C86/t51.2885-15/e35/s320x320/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 320w,https://instagram.fbah5-1.fna.fbcdn.net/vp/2e379ff408f5fc7f480b226d199840d7/5E4301DC/t51.2885-15/e35/s480x480/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 480w,https://instagram.fbah5-1.fna.fbcdn.net/vp/74d890cda269b521e7e834777bfd4ea8/5E27D9D1/t51.2885-15/sh0.08/e35/s640x640/73527623_158166208601186_7855403223712441155_n.jpg?_nc_ht=instagram.fbah5-1.fna.fbcdn.net&_nc_cat=102 640w" class="w-100"> </div>
   </div>
</div>
@endsection
