<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






/**
 * C    POST /project/create (create)
 * 
 * R    GET  /project(index)
 *      GET   /project/1/edit (show)     
 *      GET     /project/1    
 * 
 * U    PATCH /project/1 (store)
 * 
 * D    DELETE /project/1(destroy)
 *          
 * 
 * 
 */






Auth::routes();

Route::post('/follow/{user}','FollowsController@store');

Route::get('/profile/{user}/edit', 'profilesCOntroller@edit')->name('profile.edit');
Route::get('/profile/{user}', 'profilesCOntroller@index')->name('profile.show');
Route::patch('/profile/{user}', 'profilesCOntroller@update')->name('profile.update');


Route::get('/', 'PostsController@index');
Route::get('/p/explore', 'PostsController@explore');
Route::get('/p/create', 'PostsController@create');
Route::post('/p', 'PostsController@store');
Route::get('/p/{post}', 'PostsController@show');


